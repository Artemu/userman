<?php namespace app\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class UsersController extends Controller
{
    public function __construct()
    {
        // Currently logged in user
        $user = \Auth::user();

        // Verify user is authenticated
        if(!$user->hasRole('admin'))
            return abort(403, 'User has no admin role'); // There is no 403 er
    }
    public function getShow()
    {
        // Show list of all users. This list is not paginated
        $users = User::all();

        return view('users.show')->with('users', $users);
    }

    public function getAdd()
    {
        // Show form
        return view('users.add');
    }

    public function postAdd()
    {
        $input = Input::all();

        // Validate
        $validator = Validator::make($input, [ // based on laravel auth validation
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        // Verify Validation Pass
        if (!$validator->fails())
        {
            $success = User::create([ // based on laravel auth
                'name' => $input['name'],
                'surname' => $input['surname'],
                'email' => $input['email'],
                'password' => bcrypt($input['password']),
            ]);
        }
        else
            $success = false;

        // Show success or fail message
        if($success) return redirect('users/show')->with('success','User Added');
        else return view('users.add')->withErrors($validator);
    }

    public function getEdit($user)
    {
        // Load edit form
        $user = User::find($user);

        // Verify valid user
        if(empty($user->id)) return redirect('users/show')->with('error','No valid user found?');
        else return view('users.edit')->with('user', $user);
    }

    public function postEdit($user)
    {
        // Get inputs & user
        $user = User::find($user);
        $input = Input::all();

        // Validate inputs
        $validator = Validator::make($input, [ // based on laravel auth validation
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'confirmed|min:6',
        ]);

        // If success...
        if (!$validator->fails())
        {
            // Empty or Encrypt
            if(!empty($input['password']))
                $input['password'] = bcrypt($input['password']);
            else
                unset($input['password']);

            // Update the user now
            $user->update($input);
            return redirect('users/show')->with('success', 'User Updated');
        }
        else
            return view('users.edit')->with('user', $user)->withErrors($validator);
    }

    public function getDelete($user)
    {
        // Secondary verification is required for a production system. Excluded for this sample.
        $user = User::find($user);

        if(isset($user))
        {
            $user->delete();
            return redirect('users/show')->with('success','User has been deleted');
        }
        else
            return redirect('users/show')->with('error','User could not be deleted');
    }
}