@extends('app')

@section('content')

    <div class="pull-right" style="margin: 10px 0;">
        <a href="{{action('UsersController@getAdd')}}" class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></a>
    </div>
    <table class="table">
        <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>Email</th>
            <th></th>
        </tr>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>
                <td>{{$user->surname}}</td>
                <td>{{$user->email}}</td>
                <td class="text-right">
                    <a href="{{action('UsersController@getEdit',array($user->id))}}" class="btn btn-primary btn-xs">Edit User</a>
                    <a href="{{action('UsersController@getDelete',array($user->id))}}" class="btn btn-danger btn-xs">Delete User</a>
                </td>
            </tr>
        @endforeach
    </table>
@endsection